/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Slots,
    castToNumber,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

export interface IDateTimeCondition {
    mode: TMode;
    value?: number;
    to?: number;
}

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class DateTimeCondition extends ConditionBlock<IDateTimeCondition> {
    @condition
    verify(): boolean {
        const dateSlot = this.valueOf<number, Slots.Date>();

        if (dateSlot) {
            switch (this.props.mode) {
                case "equal":
                    return (
                        dateSlot.hasValue &&
                        dateSlot.slot.toValue(dateSlot.value) ===
                            dateSlot.slot.toValue(this.props.value)
                    );
                case "before":
                    return (
                        dateSlot.hasValue &&
                        dateSlot.value < dateSlot.slot.toValue(this.props.value)
                    );
                case "after":
                    return (
                        dateSlot.hasValue &&
                        dateSlot.value > dateSlot.slot.toValue(this.props.value)
                    );
                case "between":
                    return (
                        dateSlot.hasValue &&
                        dateSlot.value >=
                            dateSlot.slot.toValue(this.props.value) &&
                        dateSlot.value <= dateSlot.slot.toValue(this.props.to)
                    );
                case "defined":
                    return dateSlot.hasValue;
                case "undefined":
                    return !dateSlot.hasValue;
            }
        }

        return false;
    }
}
