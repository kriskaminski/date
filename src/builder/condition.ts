/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    L10n,
    Slots,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { TMode } from "../runner/mode";
import { IDateTimeCondition } from "../runner/condition";
import { DateTime } from "./";

/** Assets */
import ICON from "../../assets/condition.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: DateTime,
    icon: ICON,
    get label() {
        return pgettext("block:date", "Verify date");
    },
})
export class DateTimeCondition
    extends ConditionBlock
    implements IDateTimeCondition {
    @definition
    @affects("#name")
    mode: TMode = "equal";

    @definition
    @affects("#name")
    value?: number;

    @definition
    @affects("#name")
    to?: number;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Date) {
            const value =
                this.value &&
                (slot.supportsTime
                    ? L10n.locale.dateTimeShort(slot.toValue(this.value), true)
                    : L10n.locale.dateShort(slot.toValue(this.value), true));

            switch (this.mode) {
                case "between":
                    const to =
                        this.to &&
                        (slot.supportsTime
                            ? L10n.locale.dateTimeShort(
                                  slot.toValue(this.to),
                                  true
                              )
                            : L10n.locale.dateShort(
                                  slot.toValue(this.to),
                                  true
                              ));

                    return `${value ? `\`${value}\`` : "\\_"} ≤ @${slot.id} ≤ ${
                        to ? `\`${to}\`` : "\\_"
                    }`;
                case "defined":
                    return `@${slot.id} ${pgettext("block:date", "specified")}`;
                case "undefined":
                    return `@${slot.id} ${pgettext(
                        "block:date",
                        "not specified"
                    )}`;
                case "before":
                case "after":
                case "equal":
                    return `@${slot.id} ${
                        this.mode === "after"
                            ? pgettext("block:date", "after")
                            : this.mode === "before"
                            ? pgettext("block:date", "before")
                            : "="
                    } ${value ? `\`${value}\`` : "\\_"}`;
            }
        }

        return this.type.label;
    }

    @editor
    defineEditor(): void {
        const features =
            Forms.DateTimeFeatures.Date |
            Forms.DateTimeFeatures.Weekday |
            (this.slot instanceof Slots.Date && this.slot.supportsTime
                ? Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                : 0);

        const dateTo = new Forms.DateTime(
            Forms.DateTime.bind(this, "to", undefined, DateTime.getToday("end"))
        )
            .zone("UTC")
            .features(features)
            .years(
                new Date().getFullYear() - 150,
                new Date().getFullYear() + 50
            )
            .width("full")
            .required()
            .visible(this.mode === "between");

        const group = new Forms.Group([
            new Forms.DateTime(
                Forms.DateTime.bind(
                    this,
                    "value",
                    undefined,
                    DateTime.getToday("begin")
                )
            )
                .zone("UTC")
                .features(features)
                .years(
                    new Date().getFullYear() - 150,
                    new Date().getFullYear() + 50
                )
                .width("full")
                .required(),
            dateTo,
        ]).visible(this.mode !== "defined" && this.mode !== "undefined");

        this.editor.form({
            title: pgettext("block:date", "When date:"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext("block:date", "Is equal to"),
                            value: "equal",
                        },
                        {
                            label: pgettext("block:date", "Is before"),
                            value: "before",
                        },
                        {
                            label: pgettext("block:date", "Is after"),
                            value: "after",
                        },
                        {
                            label: pgettext("block:date", "Is between"),
                            value: "between",
                        },
                        {
                            label: pgettext("block:date", "Is specified"),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:date", "Is not specified"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    group.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                    dateTo.visible(mode.value === "between");
                }),
                group,
            ],
        });
    }
}
