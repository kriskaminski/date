export type TMode =
    | "equal"
    | "before"
    | "after"
    | "between"
    | "defined"
    | "undefined";
