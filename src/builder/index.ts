/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Forms,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    isBoolean,
    isNumber,
    pgettext,
    slotInsertAction,
    slots,
    tripetto,
} from "tripetto";
import { IDateTime } from "../runner";
import { DateTimeCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:date", "Date");
    },
})
export class DateTime extends NodeBlock implements IDateTime {
    dateSlot!: Slots.Date;

    @definition
    @affects("#label")
    @affects("#slots")
    time?: boolean;

    @definition
    @affects("#label")
    @affects("#slots")
    range?: boolean;

    @definition
    @affects("#slots")
    minimum?: number;

    @definition
    @affects("#slots")
    maximum?: number;

    @definition
    @affects("#slots")
    required?: boolean;

    @definition
    @affects("#slots")
    alias?: string;

    @definition
    @affects("#slots")
    exportable?: boolean;

    @definition
    placeholder?: string;

    get label() {
        if (this.range) {
            return pgettext("block:date", "Date range");
        }

        if (this.time) {
            return pgettext("block:date", "Date with time");
        }

        return this.type.label;
    }

    static getToday(s: "begin" | "end"): number {
        const today = new Date();

        today.setUTCHours(0);
        today.setUTCMinutes(0);
        today.setUTCSeconds(0);
        today.setUTCMilliseconds(0);

        return today.getTime() + (s === "end" ? 86400000 - 1 : 0);
    }

    @slots
    defineSlot(): void {
        this.dateSlot = this.slots.static({
            type: Slots.Date,
            reference: "date",
            label: this.range
                ? pgettext("block:date", "Date from")
                : this.time
                ? pgettext("block:date", "Date with time")
                : DateTime.label,
            required: this.required,
            alias: this.alias,
            exportable: this.exportable,
        });

        this.dateSlot.precision = this.time ? "minutes" : "days";
        this.dateSlot.minimum = this.minimum;
        this.dateSlot.maximum = this.maximum;

        if (this.range) {
            const toSlot = this.slots.static({
                type: Slots.Date,
                reference: "to",
                label: pgettext("block:date", "Date to"),
                required: this.required,
                alias: this.alias,
                exportable: this.exportable,
            });

            toSlot.precision = this.dateSlot.precision;
            toSlot.minimum = this.dateSlot.minimum;
            toSlot.maximum = this.dateSlot.maximum;
        } else {
            this.slots.delete("to", "static");
        }
    }

    @editor
    defineEditor(): void {
        const placeholder = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "placeholder", undefined, "")
        )
            .placeholder(
                pgettext(
                    "tripetto:date",
                    "Type placeholder for the range to date field..."
                )
            )
            .action("@", slotInsertAction(this))
            .visible(this.range || false);

        this.editor.name();
        this.editor.description();
        this.editor.placeholder(placeholder);
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:date", "Time"),
            form: {
                title: pgettext("block:date", "Time"),
                controls: [
                    new Forms.Checkbox(
                        pgettext("block:date", "Allow a time to be set"),
                        Forms.Checkbox.bind(this, "time", undefined, true)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:date",
                            "When enabled this will allow the user to set a specific time along with the date."
                        )
                    ),
                ],
            },
            activated: isBoolean(this.time),
        });

        this.editor.option({
            name: pgettext("block:date", "Range"),
            form: {
                title: pgettext("block:date", "Range"),
                controls: [
                    new Forms.Checkbox(
                        pgettext("block:date", "Enable date range"),
                        Forms.Checkbox.bind(this, "range", undefined, true)
                    ).on(() => {
                        placeholder.visible(this.range || false);
                    }),
                    new Forms.Static(
                        pgettext(
                            "block:date",
                            "When enabled this will display two date input fields. One for the *from* date and another one for the *to* date."
                        )
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.range),
        });

        this.editor.option({
            name: pgettext("block:date", "Limit values"),
            form: {
                title: pgettext("block:date", "Limit values"),
                controls: [
                    new Forms.DateTime(
                        Forms.DateTime.bind(
                            this,
                            "minimum",
                            undefined,
                            DateTime.getToday("begin")
                        )
                    )
                        .zone("UTC")
                        .features(
                            Forms.DateTimeFeatures.Date |
                                Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                        )
                        .years(
                            new Date().getFullYear() - 150,
                            new Date().getFullYear() + 50
                        )
                        .label(pgettext("block:date", "Minimum"))
                        .placeholder(pgettext("block:date", "Not set")),
                    new Forms.DateTime(
                        Forms.DateTime.bind(
                            this,
                            "maximum",
                            undefined,
                            DateTime.getToday("end")
                        )
                    )
                        .zone("UTC")
                        .features(
                            Forms.DateTimeFeatures.Date |
                                Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                        )
                        .years(
                            new Date().getFullYear() - 150,
                            new Date().getFullYear() + 50
                        )
                        .label(pgettext("block:date", "Maximum"))
                        .placeholder(pgettext("block:date", "Not set")),
                ],
            },
            activated: isNumber(this.minimum) || isNumber(this.maximum),
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();
        this.editor.alias(this);
        this.editor.exportable(this);
    }

    @conditions
    defineCondition(): void {
        const date = this.slots.select("date");
        const to = this.slots.select("to");
        const templates = [
            {
                mode: "equal" as "equal",
                label: pgettext("block:date", "Is equal to"),
            },
            {
                mode: "before" as "before",
                label: pgettext("block:date", "Is before"),
            },
            {
                mode: "after" as "after",
                label: pgettext("block:date", "Is after"),
            },
            {
                mode: "between" as "between",
                label: pgettext("block:date", "Is between"),
            },
            {
                mode: "defined" as "defined",
                label: pgettext("block:date", "Is specified"),
            },
            {
                mode: "undefined" as "undefined",
                label: pgettext("block:date", "Is not specified"),
            },
        ];

        if (date) {
            each(templates, (condition) => {
                this.conditions.template({
                    condition: DateTimeCondition,
                    label: to
                        ? `${date.label} /  ${condition.label}`
                        : condition.label,
                    props: {
                        slot: date,
                        mode: condition.mode,
                    },
                });
            });
        }

        if (to) {
            each(templates, (condition) => {
                this.conditions.template({
                    condition: DateTimeCondition,
                    label: `${to.label} / ${condition.label}`,
                    props: {
                        slot: to,
                        mode: condition.mode,
                    },
                });
            });
        }
    }
}
